/*********************************
* Class: MAGSHIMIM C2			 *
* Week:                			 *
* Name:                          *
* Credits:                       *
**********************************/

#include <stdio.h>
#include <stdlib.h>
#include <dirent.h> // FOR TESTER, CHECK IF INSTALLED
#include <errno.h> // only for bug checking, not used in final project --> printf("Error: %s", strerror(errno));
#include <string.h>


#define TRUE 1
#define FALSE 0
#define MAX_CONTENT_SIZE 500000
#define MAX_PATH_SIZE 250
#define QUICK_SCAN_FIRST_PART 0.2
#define QUICK_SCAN_SECOND_PART 0.8
#define FOUND_FIRST_PART 1
#define FOUND_SECOND_PART 2
#define FOUND_IN_ALL_FILE 3

int countFiles(DIR* dir);
int isInfected(char* virusSigneture, char* fileContent, int virusSize, int fileSize);
int isInfectedQuick(char* virusSigneture, char* fileContent, int virusSize, int fileSize);
int getFileSize(FILE* fp);
int compareBinaryStrings(char* b1, char* b2, int len);
void findSubString(int start, int stop, char* string, char* subString);
void printBinary(char* binaryString, int size);
void checkVirusDirNormal(char* virusSigneture, char* dirPath, int virusSize, FILE* logFile);
void checkVirusDirQuick(char* virusSigneture, char* dirPath, int virusSize, FILE* logFile);
void logDetailsWrite(FILE* log, char* dirPath, char* virusPath, int scanningMode);

int main(int argc, char* argv[])
{
	unsigned char virusPath[MAX_PATH_SIZE] = { 0 }, dirCheckPath[MAX_PATH_SIZE] = { 0 }; // paths to files 
	unsigned char* virusSigneture = 0;
	char logPath[MAX_PATH_SIZE] = { 0 };
	int virusFileSize = 0, scanningMode = 0;
	FILE* virusFile = 0;
	FILE* logFile = 0;
	// getting the paths:
	printf("Welcome to my virus scan!\n\n");
	strcpy(dirCheckPath, argv[1]); // first we get folder to scan
	printf("Folder to scan: %s\n", dirCheckPath);
	strcpy(virusPath, argv[2]);
	printf("Virus signeture: %s\n", virusPath);
	// set log file name:
	strcpy(logPath, dirCheckPath); // it will contain only dir path
	strcat(logPath, "\\Anti_Virus_Log.txt");
	// scanning mode:
	printf("Press 0 for normal scan or any other key for a quick scan: ");
	scanf("%d", &scanningMode);
	// reading virus signeture
	virusFile = fopen(virusPath, "rb");
	// if there is no such file for virus
	if (virusFile == NULL)
	{
		printf("There is no such file for virus signeture!\n");
		getchar(); // wait for user to read
		getchar();
		exit(1);
	}
	virusFileSize = getFileSize(virusFile);
	virusSigneture = (unsigned char*)malloc(sizeof(unsigned char) * virusFileSize);
	fread(virusSigneture, virusFileSize, 1, virusFile);
	fclose(virusFile);
	// log information write:
	logFile = fopen(logPath, "wt");
	logDetailsWrite(logFile, dirCheckPath, virusPath, scanningMode);
	fclose(logFile);
	printf("\nBegan scanning...\n");
	logFile = fopen(logPath, "at"); // only adding text from now on
	if (scanningMode == 0)
	{
		checkVirusDirNormal(virusSigneture, dirCheckPath, virusFileSize, logFile);
	}
	else
	{
		checkVirusDirQuick(virusSigneture, dirCheckPath, virusFileSize, logFile);
	}
	fclose(logFile);
	printf("Scan completed!\nSee log path for results: %s", logPath);
	free(virusSigneture);
	getchar();
	getchar();
	return 0;
}

/*
Function gets two binary strings and checks if the first one is inside the second
Input: virusSigneture, file content and their length
Output: TRUE or FALSE
*/
int isInfected(char* virusSigneture, char* fileContent, int virusSize, int fileSize)
{
	// we will go through chunks in file and compare them to the virus
	char* checkedSubString = (char*)malloc(virusSize * sizeof(char)); 
	int i = 0;
	int isInfected = FALSE;
	for (i = 0; i < fileSize - virusSize + 1; i++) // loop through file
	{
		findSubString(i, i + virusSize, fileContent, checkedSubString);
		if (compareBinaryStrings(checkedSubString, virusSigneture, virusSize)) // compare virus and chunk
		{
			isInfected = TRUE;
		}
	}
	free(checkedSubString);
	return isInfected;

}

/*
Function gets two binary strings and checks if the first one is inside the second
Input: virusSigneture, file content and their length
Output: 0 - not found, 1 - found in first 1/5, 2 - found in fourth 1/5 (4/5), 3 - found in file
*/
int isInfectedQuick(char* virusSigneture, char* fileContent, int virusSize, int fileSize)
{
	// we will go through chunks in file and compare them to the virus
	char* checkedSubString = (char*)malloc(virusSize * sizeof(char));
	int i = 0;
	int isInfected = FALSE;
	// loop through first part of file
	for (i = 0; i < (fileSize - virusSize + 1) * QUICK_SCAN_FIRST_PART; i++) // loop through file
	{
		findSubString(i, i + virusSize, fileContent, checkedSubString);
		if (compareBinaryStrings(checkedSubString, virusSigneture, virusSize)) // compare virus and chunk
		{
			isInfected = FOUND_FIRST_PART;
		}
	}
	// loop through second
	if (isInfected == FALSE)
	{
		for (i = (fileSize - virusSize + 1) * QUICK_SCAN_SECOND_PART; i < (fileSize - virusSize); i++) // loop through file
		{
			findSubString(i, i + virusSize, fileContent, checkedSubString);
			if (compareBinaryStrings(checkedSubString, virusSigneture, virusSize)) // compare virus and chunk
			{
				isInfected = FOUND_SECOND_PART;
			}
		}
	}
	// loop through all file
	if (isInfected == FALSE)
	{
		for (i = 0; i < fileSize - virusSize + 1; i++) // loop through file
		{
			findSubString(i, i + virusSize, fileContent, checkedSubString);
			if (compareBinaryStrings(checkedSubString, virusSigneture, virusSize)) // compare virus and chunk
			{
				isInfected = FOUND_IN_ALL_FILE;
			}
		}
	}
	free(checkedSubString);
	return isInfected;

}

/*
Function prints a binary string
Input: binary string and its size
Output: None
*/
void printBinary(char* binaryString, int size)
{
	int i = 0;
	for (i = 0; i < size; i++)
	{
		printf("%hhx ", binaryString[i]);
	}
}

/*
Function checks file's size
input: FILE stream
output: file's size
*/
int getFileSize(FILE* fp)
{
	int fileSize = 0; // in bytes
	fseek(fp, 0, SEEK_END); // move to the end of file
	fileSize = ftell(fp); // check where we got moved
	rewind(fp); // back the seeker to the start of the file
	return fileSize;
}

/*
Function compares two binary strings with same length
Input: two binary string and there SAME length
output: TRUE or FALSE
*/
int compareBinaryStrings(char* b1, char* b2, int len)
{
	int areSame = 1;
	int i = 0;
	for (i = 0; i < len; i++)
	{
		if (b1[i] != b2[i])
		{
			areSame = 0;
		}
	}
	return areSame;
}

/*
Function finds a sub string in a string with given indexes
Input: start, stop, a string and a substring pointer (where to store the given substring)
Output: None
*/
void findSubString(int start, int stop, char* string, char* subString)
{
	int i = 0;
	int indexInSubString = 0;
	for (i = start; i < stop; i++)
	{
		subString[indexInSubString] = string[i];
		indexInSubString++;
	}
}

/*
Function prints and adds to log outcome of checking if a directory has infected file in normal scan mode
Input: virus signature, directory path, virus size and log file stream
Output: None 
*/
void checkVirusDirNormal(char* virusSigneture, char* dirPath, int virusSize, FILE* logFile)
{
	DIR *dir = 0;
	FILE* checkFile = 0;
	struct dirent *ent = (struct dirent *)!NULL; // not null
	char fileName[MAX_PATH_SIZE] = { 0 };
	char* fileContent = 0;
	char filePath[MAX_PATH_SIZE] = { 0 };
	char logLineAdd[MAX_PATH_SIZE] = { 0 };
	int fileSize = 0;
	// open directory
	dir = opendir(dirPath);	
	if (dir != NULL) // check for if its found
	{
		while (ent != NULL)
		{
			strcpy(filePath, dirPath); // it will contain only dir path
			strcat(filePath, "\\");
			logLineAdd[0] = 0; // restart the string
			ent = readdir(dir);
			if (ent != NULL)
			{
				if (!(strcmp(ent->d_name, ".") == 0 || strcmp(ent->d_name, "..") == 0)) // filter out the dots 
				{
					strcpy(fileName, ent->d_name); // get the file name
					strcat(filePath, fileName); // here we get full path
					checkFile = fopen(filePath, "rb"); // read the file
					fileSize = getFileSize(checkFile); // get its size
					fileContent = (char*)malloc(fileSize * sizeof(char));
					fread(fileContent, fileSize, 1, checkFile); // read the file
					fclose(checkFile);
					strcat(logLineAdd, filePath); // add to log line the path
					// check if infected:
					if (isInfected(virusSigneture, fileContent, virusSize, fileSize))
					{ // if file is infected 
						printf("%s - Infected!\n", filePath);
						// adding to log file
						strcat(logLineAdd, " - Infected!\n");
					}
					else
					{
						printf("%s - Clean\n", filePath);
						// adding to log file
						strcat(logLineAdd, " - Clean\n");
					}
					// add to log file:
					fwrite(logLineAdd, strlen(logLineAdd), 1, logFile);
					free(fileContent);
				}
			}
		}
	}
	else
	{
		printf("No directory found!");
	}
	closedir(dir);
}

/*
Function prints and adds to log outcome of checking if a directory has infected file in quick scan mode
Input: virus signature, directory path, virus size and log file stream
Output: None
*/
void checkVirusDirQuick(char* virusSigneture, char* dirPath, int virusSize, FILE* logFile)
{
	DIR *dir = 0;
	FILE* checkFile = 0;
	struct dirent *ent = (struct dirent *)!NULL; // not null
	char fileName[MAX_PATH_SIZE] = { 0 };
	char* fileContent = 0;
	char filePath[MAX_PATH_SIZE] = { 0 };
	char logLineAdd[MAX_PATH_SIZE] = { 0 };
	int fileSize = 0;
	int dirFileCount = 0, currectFileNumber = 0;
	// open directory
	dir = opendir(dirPath);
	dirFileCount = countFiles(dir);
	if (dir != NULL) // check for if its found
	{
		rewinddir(dir); // get seeker to start
		while (ent != NULL)
		{
			strcpy(filePath, dirPath); // it will contain only dir path
			strcat(filePath, "\\");
			logLineAdd[0] = 0; // restart the string
			ent = readdir(dir);
			if (ent != NULL)
			{
				if (!(strcmp(ent->d_name, ".") == 0 || strcmp(ent->d_name, "..") == 0)) // filter out the dots 
				{
					strcpy(fileName, ent->d_name); // get the file name
					strcat(filePath, fileName); // here we get full path
					checkFile = fopen(filePath, "rb"); // read the file
					fileSize = getFileSize(checkFile); // get its size
					fileContent = (char*)malloc(fileSize * sizeof(char));
					fread(fileContent, fileSize, 1, checkFile); // read the file
					fclose(checkFile);
					strcat(logLineAdd, filePath); // add to log line the path
					// check if infected:
					if (isInfectedQuick(virusSigneture, fileContent, virusSize, fileSize) == FOUND_FIRST_PART)
					{ // if file is infected 			
						printf("%s - Infected! <first 20%%>\n", filePath);
						// adding to log file
						strcat(logLineAdd, " - Infected! <first 20%>\n");
					}
					else if (isInfectedQuick(virusSigneture, fileContent, virusSize, fileSize) == FOUND_SECOND_PART)
					{ // if file is infected 			
						printf("%s - Infected! <last 20%%>\n", filePath);
						// adding to log file
						strcat(logLineAdd, " - Infected! <last 20%>\n");
					}
					else if (isInfectedQuick(virusSigneture, fileContent, virusSize, fileSize) == FOUND_IN_ALL_FILE)
					{ // if file is infected 			
						printf("%s - Infected!\n", filePath);
						// adding to log file
						strcat(logLineAdd, " - Infected!\n");
					}
					else
					{			
						printf("%s - Clean\n", filePath);
						// adding to log file
						strcat(logLineAdd, " - Clean\n");
					}
					// add to log file:
					fwrite(logLineAdd, strlen(logLineAdd), 1, logFile);
					free(fileContent);
					currectFileNumber++; // we moved one file further
				}
			}
		}
	}
	else
	{
		printf("No directory found!");
	}
	closedir(dir);
}


/*
Function writes details about a scan into a log file
Input: log file stream, directory path, virus path, scanning mode
Output: None
*/
void logDetailsWrite(FILE* log, char* dirPath, char* virusPath, int scanningMode)
{
	char logDetails[MAX_PATH_SIZE] = { 0 };
	strcat(logDetails, "Welcome to the antivirus! Thanks for using!\n\nFolder to scan:\n");
	strcat(logDetails, dirPath);
	strcat(logDetails, "\nVirus signature:\n");
	strcat(logDetails, virusPath);
	strcat(logDetails, "\n\nScanning option:\n");
	if (scanningMode == 0)
	{
		strcat(logDetails, "Normal Scan\n\n");
	}
	else
	{
		strcat(logDetails, "Quick Scan\n");
	}
	strcat(logDetails, "\nResults:\n");
	fwrite(logDetails, strlen(logDetails), 1, log);
}

/*
Function counts files in a directory
input: directory stream
output: number of files
*/
int countFiles(DIR* dir)
{
	int counter = 0;
	struct dirent *ent;
	ent = readdir(dir);
	while (ent != NULL)
	{
		if (ent->d_type == DT_REG) // check for a regular file and not a dot or directory
		{
			counter++;
		}
		ent = readdir(dir);
	}
	return counter;
}